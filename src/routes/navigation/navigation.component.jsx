import { Outlet, Link } from 'react-router-dom'
import { Fragment } from 'react'
import './navigation.style.scss'


const Navigation = () => {
    return (
        <Fragment>
            <div className='navigation'>
                <Link className='logo-container' to='/'>
                    <div className='logo'>LOGO</div>
                </Link>
                <div className='links-container'>
                    <Link className='nav-link' to='/shop'>SHOP</Link>
                    <Link className='nav-link' to=''>CONTACT</Link>
                    <Link className='nav-link' to=''>SIGN IN</Link>
                </div>
            </div>
            <Outlet />
        </Fragment>
    )
}

export default Navigation